package UI;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import modelMusee.Musee;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Login extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JLabel lblLogin;
	private JLabel lblMotDePasse;
	private JTextField login;
	private JTextField mdp;
	private JButton btnConnexion;
	private JButton btnAnnuler;
	private Musee musee;

	/**
	 * Create the frame.
	 */
	public Login(Musee musee) {
		this.musee = musee;
		initialize();
	}

	public void initialize() {
		setTitle("Login");
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 353, 177);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		lblLogin = new JLabel("Login :");
		lblLogin.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblLogin.setBounds(10, 11, 99, 19);
		contentPane.add(lblLogin);

		lblMotDePasse = new JLabel("Mot de passe :");
		lblMotDePasse.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblMotDePasse.setBounds(10, 41, 99, 19);
		contentPane.add(lblMotDePasse);

		login = new JTextField();
		login.setBounds(119, 12, 130, 20);
		contentPane.add(login);
		login.setColumns(10);

		mdp = new JTextField();
		mdp.setBounds(119, 42, 130, 20);
		contentPane.add(mdp);
		mdp.setColumns(10);
		mdp.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER){
					musee.login(login.getText(), mdp.getText());
					Login.this.dispose();
				}
			}
		});

		btnConnexion = new JButton("Connexion");
		btnConnexion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				musee.login(login.getText(), mdp.getText());
				Login.this.dispose();
			}
		});
		btnConnexion.setBounds(197, 89, 89, 23);
		contentPane.add(btnConnexion);

		btnAnnuler = new JButton("Annuler");
		btnAnnuler.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Login.this.dispose();
			}
		});
		btnAnnuler.setBounds(20, 89, 89, 23);
		contentPane.add(btnAnnuler);
	}

}
