package UI;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

import modelMusee.DemandePhoto;
import modelMusee.Historien;
import modelMusee.Musee;
import modelMusee.Oeuvre;

public class CreerDemandePhoto extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextArea taDemande;
	private JButton btnCrer;
	private JLabel lblOeuvre;
	private JLabel nameOeuvre;

	/**
	 * Create the frame.
	 */
	public CreerDemandePhoto(final Musee musee, final Oeuvre o) {
		setTitle("Cr\u00E9er une nouvelle demande photo");
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 295, 257);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		taDemande = new JTextArea();
		taDemande.setBounds(10, 36, 259, 120);
		contentPane.add(taDemande);

		btnCrer = new JButton("Cr\u00E9er");
		btnCrer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				musee.addListeDemandePhoto(new DemandePhoto(
						(Historien) musee.getUtilisateurSession(), o, taDemande
								.getText()));
				
				CreerDemandePhoto.this.dispose();
			}
		});
		btnCrer.setBounds(10, 166, 259, 43);
		contentPane.add(btnCrer);

		lblOeuvre = new JLabel("Oeuvre :");
		lblOeuvre.setBounds(10, 11, 46, 14);
		contentPane.add(lblOeuvre);

		nameOeuvre = new JLabel(o.getNomOeuvre());
		nameOeuvre.setBounds(66, 11, 138, 14);
		contentPane.add(nameOeuvre);
	}
}
