package UI;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextPane;

import modelMusee.DemandePhoto;

public class AffichageDP extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JLabel lblHistorien;
	private JLabel lblOeuvre;
	private JLabel lblDate;
	private JLabel lblPhotographe;
	private JLabel histoto;
	private JLabel oeuvre;
	private JLabel date;
	private JLabel photo;
	private JTextPane textPane;

	/**
	 * Create the frame.
	 */
	@SuppressWarnings("deprecation")
	public AffichageDP(DemandePhoto dp) {
		setTitle("Demande photo de "
				+ dp.getHistorienDemandePhoto().getLogin_utilisateur());
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 275, 224);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		lblHistorien = new JLabel("Historien :");
		lblHistorien.setBounds(10, 11, 58, 14);
		contentPane.add(lblHistorien);

		lblOeuvre = new JLabel("Oeuvre :");
		lblOeuvre.setBounds(10, 36, 46, 14);
		contentPane.add(lblOeuvre);

		lblDate = new JLabel("Date :");
		lblDate.setBounds(10, 61, 46, 14);
		contentPane.add(lblDate);

		lblPhotographe = new JLabel("Photographe :");
		lblPhotographe.setBounds(10, 86, 76, 14);
		contentPane.add(lblPhotographe);

		histoto = new JLabel(dp.getHistorienDemandePhoto()
				.getLogin_utilisateur());
		histoto.setBounds(88, 11, 161, 14);
		contentPane.add(histoto);

		oeuvre = new JLabel(dp.getOeuvre().getNomOeuvre());
		oeuvre.setBounds(88, 36, 161, 14);
		contentPane.add(oeuvre);

		date = new JLabel(dp.getDate_demande_photo().toLocaleString());
		date.setBounds(88, 61, 161, 14);
		contentPane.add(date);

		if (dp.getPhotographeDemandePhoto() == null) {
			photo = new JLabel("Aucun");
		} else {
			photo = new JLabel(dp.getPhotographeDemandePhoto()
					.getLogin_utilisateur());
		}
		photo.setBounds(88, 86, 161, 14);
		contentPane.add(photo);
		
		textPane = new JTextPane();
		textPane.setBounds(10, 111, 239, 64);
		textPane.setText(dp.getDescription());
		contentPane.add(textPane);
	}
}
