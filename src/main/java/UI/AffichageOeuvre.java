package UI;
import java.awt.Font;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;

import modelMusee.Oeuvre;


public class AffichageOeuvre extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6261694764420797679L;
	private JPanel contentPane;
	private JLabel lblNomoeuvre;
	private JTextPane descOeuvre;

	/**
	 * Create the frame.
	 */
	public AffichageOeuvre(Oeuvre o) {
		setTitle(o.getNomOeuvre());
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblNomoeuvre = new JLabel("nomOeuvre");
		lblNomoeuvre.setFont(new Font("Tahoma", Font.BOLD, 25));
		lblNomoeuvre.setBounds(84, 11, 269, 51);
		lblNomoeuvre.setText(o.getNomOeuvre());
		contentPane.add(lblNomoeuvre);
		
		descOeuvre = new JTextPane();
		descOeuvre.setFont(new Font("Tahoma", Font.PLAIN, 17));
		descOeuvre.setEditable(false);
		descOeuvre.setBounds(10, 90, 414, 161);
		descOeuvre.setText(o.getDescriptionOeuvre());
		contentPane.add(descOeuvre);
	}
}
