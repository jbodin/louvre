package UI;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.HashSet;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.UIManager;

import modelMusee.DemandePhoto;
import modelMusee.Departement;
import modelMusee.Historien;
import modelMusee.Musee;
import modelMusee.Oeuvre;
import modelMusee.Photo;
import modelMusee.Photographe;

public class Main {

	private static Musee musee;
	private Departement dep;
	private Main cmoi;

	private JFrame main;
	private JComboBox<Object> comboBox;
	private JButton btnActi;
	private JLabel label;
	private JButton btnAccueil;
	private JButton btnLogin;
	private JButton btnDemandePhoto;
	private JButton btnDP;
	private JButton btnVoirMesDemandes;
	private JButton btnHonorerValider;
	private JButton btnRefuserDenga;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Departement peinture = new Departement("Peinture");
					Oeuvre jo = new Oeuvre("Joconde (La)",
							"Trop connu, pas de description");
					Oeuvre tim = new Oeuvre(
							"Tim le BG",
							"Cette peinture repr�sente un Coq se nommant Tim,"
									+ " il brandit ses plumes pour s�duire les poules en arri�re plan."
									+ "\nL'effet est imm�diat et ce fait ressentir sur la ponte des �ufs.");
					peinture.addListeOeuvres(jo);
					peinture.addListeOeuvres(tim);
					Departement sculture = new Departement("Sculpture");
					sculture.addListeOeuvres(new Oeuvre("Jean-�ude",
							"Cette sculture repr�sente le tr�s c�l�bre Jean-�ude,"
									+ " la v�ritable icone de tim le coq."));
					Departement photographie = new Departement("Photographie");
					photographie.addListeOeuvres(new Oeuvre("Photo trop cool",
							"Elle est vraiment cool cette photo"));
					Historien histo1 = new Historien(01, "hoeude", "1");
					Historien histo2 = new Historien(01, "htim", "1");
					Photographe ph1 = new Photographe(02, "poeude", "1");
					DemandePhoto dp1 = new DemandePhoto(histo1, jo,
							"Je veux son nez");
					DemandePhoto dp2 = new DemandePhoto(histo2, tim,
							"Je veux une photo d�taill� des poules");
					musee = new Musee("COUCOU");
					musee.addListeDepartements(peinture);
					musee.addListeDepartements(sculture);
					musee.addListeDepartements(photographie);
					musee.addListeUtilisateur(histo1);
					musee.addListeUtilisateur(histo2);
					musee.addListeUtilisateur(ph1);
					musee.addListeDemandePhoto(dp1);
					musee.addListeDemandePhoto(dp2);

					UIManager
							.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
					Main window = new Main();
					window.main.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Main() {
		cmoi = this;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */

	/**
	 * @wbp.parser.entryPoint
	 */
	public void initialize() {
		main = new JFrame();
		main.setTitle("Le mus\u00E9e du Louvre");
		main.setResizable(false);
		main.setIconImage(Toolkit.getDefaultToolkit().getImage(
				"C:\\Users\\Joris\\Downloads\\joconde.jpg"));
		main.setBounds(300, 200, 450, 300);
		main.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		main.getContentPane().setLayout(null);

		comboBox = new JComboBox<Object>();
		comboBox.setBounds(109, 40, 226, 56);
		afficheListDepartement();

		main.getContentPane().add(comboBox);

		btnActi = new JButton("Rechercher");
		btnActi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				afficherOeuvreDepartement();
			}
		});
		btnActi.setBounds(109, 107, 226, 56);
		main.getContentPane().add(btnActi);

		label = new JLabel("Selectionner un d\u00E9partement SVP");
		label.setBounds(126, 15, 174, 14);
		main.getContentPane().add(label);

		btnAccueil = new JButton("Accueil");
		btnAccueil.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				main.dispose();
				initialize();
				main.setVisible(true);
			}
		});
		btnAccueil.setBounds(345, 11, 89, 23);
		main.getContentPane().add(btnAccueil);

		btnLogin = new JButton("Login");
		if (musee.getUtilisateurSession() != null)
			btnLogin.setText(musee.getUtilisateurSession()
					.getLogin_utilisateur());
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				connexion();
			}
		});
		btnLogin.setBounds(10, 11, 89, 23);
		main.getContentPane().add(btnLogin);

		btnDemandePhoto = new JButton("Demandes photos");
		if (musee.getUtilisateurSession() == null) {
			btnDemandePhoto.setVisible(false);
		} else {
			btnDemandePhoto.setVisible(true);
		}
		btnDemandePhoto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				afficheListeDemandePhoto();
			}
		});
		btnDemandePhoto.setBackground(Color.YELLOW);
		btnDemandePhoto.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		btnDemandePhoto.setBounds(10, 168, 209, 93);
		main.getContentPane().add(btnDemandePhoto);

		btnDP = new JButton("Ca depend");
		btnDP.setVisible(false);
		btnDP.setBackground(Color.RED);
		btnDP.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		btnDP.setBounds(229, 168, 205, 93);
		main.getContentPane().add(btnDP);

		btnVoirMesDemandes = new JButton(
				"<HTML><BODY>Voir mes<BR> demandes</BODY></HTML>");
		btnVoirMesDemandes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				afficheSesDemandesTravauxPhoto();
			}
		});
		btnVoirMesDemandes.setVisible(false);
		btnVoirMesDemandes.setBounds(345, 40, 89, 56);
		btnVoirMesDemandes.setBackground(Color.GREEN);
		main.getContentPane().add(btnVoirMesDemandes);

		btnHonorerValider = new JButton("Ca depend");
		btnHonorerValider.setVisible(false);
		btnHonorerValider.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DemandePhoto dp = (DemandePhoto) comboBox.getSelectedItem();
				if (musee.getUtilisateurSession().getClass() == Historien.class) {
					dp.getEdp().validerDemandePhoto();
				} else {
					dp.getEdp().honorerDemandePhoto(cmoi);
				}
			}
		});
		btnHonorerValider.setBounds(10, 40, 89, 56);
		main.getContentPane().add(btnHonorerValider);

		btnRefuserDenga = new JButton("Ca depend");
		btnRefuserDenga.setVisible(false);
		btnRefuserDenga.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DemandePhoto dp = (DemandePhoto) comboBox.getSelectedItem();
				if (musee.getUtilisateurSession().getClass() == Historien.class) {
					dp.getEdp().modifierDemandePhoto(cmoi);
				} else {
					dp.getEdp().desengagerDemandePhoto();
				}
			}
		});
		btnRefuserDenga.setBounds(10, 107, 89, 56);
		main.getContentPane().add(btnRefuserDenga);
	}

	private void afficherOeuvreDepartement() {
		dep = (Departement) comboBox.getSelectedItem();
		comboBox.removeAllItems();
		System.out.println(dep.getListLettreOeuvre());
		for (char c : dep.getListLettreOeuvre()) {
			comboBox.addItem(c);
		}
		label.setText("Selectionner une lettre");

		for (ActionListener al : btnActi.getActionListeners()) {
			btnActi.removeActionListener(al);
		}

		btnActi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnActi.setText("Visualiser");
				char c = (Character) comboBox.getSelectedItem();
				comboBox.removeAllItems();
				System.out.println(dep.getListOeuvre(c));
				for (Oeuvre o : dep.getListOeuvre(c)) {
					comboBox.addItem(o);
				}
				label.setText("Selectionner une oeuvre");

				for (ActionListener al : btnActi.getActionListeners()) {
					btnActi.removeActionListener(al);
				}

				for (ActionListener al : btnDP.getActionListeners()) {
					btnDP.removeActionListener(al);
				}

				if (musee.getUtilisateurSession() != null) {
					if (musee.getUtilisateurSession().getClass() == Historien.class) {
						btnDP.setText("Creer demande photo");
						btnDP.setVisible(true);
						btnDP.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								creerDemandePhoto();
							}
						});
					}
				}

				btnActi.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						AffichageOeuvre dialog = new AffichageOeuvre(
								(Oeuvre) comboBox.getSelectedItem());
						dialog.setModal(true);
						dialog.setVisible(true);
					}
				});
			}
		});

	}

	private void connexion() {
		Login lolo = new Login(musee);
		lolo.setModal(true);
		lolo.setVisible(true);
		if (musee.getUtilisateurSession() != null) {
			btnLogin.setText(musee.getUtilisateurSession()
					.getLogin_utilisateur());
			btnDemandePhoto.setVisible(true);
		}
	}

	private void creerDemandePhoto() {
		CreerDemandePhoto dialog = new CreerDemandePhoto(musee,
				(Oeuvre) comboBox.getSelectedItem());
		dialog.setModal(true);
		dialog.setVisible(true);
	}

	private void afficheListDepartement() {
		for (Departement d : musee.listeDepartements) {
			comboBox.addItem(d);
		}
	}

	private void afficheListeDemandePhoto() {
		btnVoirMesDemandes.setVisible(true);
		comboBox.removeAllItems();
		for (DemandePhoto dp : musee.listeDemandePhoto) {
			comboBox.addItem(dp);
		}
		label.setText("Selectionner demande photo");

		for (ActionListener al : btnActi.getActionListeners()) {
			btnActi.removeActionListener(al);
		}

		for (ActionListener al : btnDP.getActionListeners()) {
			btnDP.removeActionListener(al);
		}

		btnActi.setText("Visualiser");

		if (musee.getUtilisateurSession() != null) {
			if (musee.getUtilisateurSession().getClass() == Photographe.class) {
				btnDP.setVisible(true);
				btnDP.setText("Accepter demande");
				btnDP.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						accepterDemandePhoto();
					}
				});
			}
		}

		btnActi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DemandePhoto dp = (DemandePhoto) comboBox.getSelectedItem();
				afficheDemandeTravauxPhoto(dp);
			}
		});
	}

	private void accepterDemandePhoto() {
		DemandePhoto dp = (DemandePhoto) comboBox.getSelectedItem();
		musee.addPhotographeDemandePhoto(dp,
				(Photographe) musee.getUtilisateurSession());

	}

	private void afficheSesDemandesTravauxPhoto() {
		comboBox.removeAllItems();
		btnHonorerValider.setVisible(true);
		btnRefuserDenga.setVisible(true);

		if (musee.getUtilisateurSession().getClass() == Historien.class) {
			for (DemandePhoto dp : musee
					.getDemandePhotoHistorien((Historien) musee
							.getUtilisateurSession())) {
				comboBox.addItem(dp);
			}
			btnHonorerValider.setText("Valider");
			btnRefuserDenga.setText("Refuser");
		} else {
			for (DemandePhoto dp : musee
					.getDemandePhotoPhotographe((Photographe) musee
							.getUtilisateurSession())) {
				comboBox.addItem(dp);
			}
			btnHonorerValider.setText("Honorer");
			btnRefuserDenga.setText("Annuler");
		}
	}

	private void afficheDemandeTravauxPhoto(DemandePhoto dp) {
		AffichageDP aDP = new AffichageDP(dp);
		aDP.setModal(true);
		aDP.setVisible(true);
	}

	public void afficheHonorerDemande(DemandePhoto dp) {
		Collection<Photo> photos = new HashSet<Photo>();
		photos.add(new Photo("elle est belleeee", null));
		dp.getEdp().validerHonorerDemandePhoto(photos);
	}

	public void afficheModifDemandeRefusee(DemandePhoto dp) {
		dp.setDescription(dp.getDescription()+"\nFranchement c'est tout naze tes photos");
		dp.getEdp().validerModifDemandePhoto();
	}
}
