package modelMusee;

/***********************************************************************
 * Module: Utilisateur.java
 * Author: Tim et Joris
 * Purpose: Defines the Class Utilisateur
 ***********************************************************************/

public class Utilisateur {
	private int ID_utilisateur;
	private String login_utilisateur;
	private String mot_de_passe_utilisateur;

	public Utilisateur(int iD_utilisateur, String login_utilisateur,
			String mot_de_passe_utilisateur) {
		this.ID_utilisateur = iD_utilisateur;
		this.login_utilisateur = login_utilisateur;
		this.mot_de_passe_utilisateur = mot_de_passe_utilisateur;
	}

	public int getID_utilisateur() {
		return ID_utilisateur;
	}

	public void setID_utilisateur(int iD_utilisateur) {
		ID_utilisateur = iD_utilisateur;
	}

	public String getLogin_utilisateur() {
		return login_utilisateur;
	}

	public void setLogin_utilisateur(String login_utilisateur) {
		this.login_utilisateur = login_utilisateur;
	}

	public Boolean verificationLogin(String login, String motDePasse) {
		return (login.equals(login_utilisateur))
				&& (motDePasse.equals(mot_de_passe_utilisateur));
	}
}