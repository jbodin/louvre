package modelMusee;

/***********************************************************************
 * Module:  Musee.java
 * Author:  Tim et Joris
 * Purpose: Defines the Class Musee
 ***********************************************************************/

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

public class Musee {
	private String nomMusee;
	private Utilisateur utilisateurSession;

	public Collection<Departement> listeDepartements;
	public Collection<DemandePhoto> listeDemandePhoto;
	public Collection<Utilisateur> listeUtilisateur;

	public Musee(String nomMusee) {
		this.nomMusee = nomMusee;
	}

	public void login(String login, String motDePasse) {
		for (Utilisateur u : getListeUtilisateur()) {
			if (u.verificationLogin(login, motDePasse)) {
				setUtilisateurSession(u);
				System.out.println("Utilisateur connect�: "
						+ u.getLogin_utilisateur());
			}
		}
	}

	public void addPhotographeDemandePhoto(DemandePhoto dp, Photographe p) {
		dp.prendreEnCharge(p);
	}

	public String getNomMusee() {
		return nomMusee;
	}

	public void setNomMusee(String nomMusee) {
		this.nomMusee = nomMusee;
	}

	public Utilisateur getUtilisateurSession() {
		return utilisateurSession;
	}

	public void setUtilisateurSession(Utilisateur utilisateurSession) {
		this.utilisateurSession = utilisateurSession;
	}

	public Collection<DemandePhoto> getDemandePhotoHistorien(Historien histo) {
		return histo.getListeDemandePhotoHistorien();
	}

	public Collection<DemandePhoto> getDemandePhotoPhotographe(Photographe photo) {
		return photo.getListeDemandePhotoPhotographe();
	}

	public Collection<Departement> getListeDepartements() {
		if (listeDepartements == null)
			listeDepartements = new HashSet<Departement>();
		return listeDepartements;
	}

	public Iterator<Departement> getIteratorListeDepartements() {
		if (listeDepartements == null)
			listeDepartements = new HashSet<Departement>();
		return listeDepartements.iterator();
	}

	public void setListeDepartements(
			Collection<Departement> newListeDepartements) {
		removeAllListeDepartements();
		for (Iterator<Departement> iter = newListeDepartements.iterator(); iter
				.hasNext();)
			addListeDepartements((Departement) iter.next());
	}

	public void addListeDepartements(Departement newDepartement) {
		if (newDepartement == null)
			return;
		if (this.listeDepartements == null)
			this.listeDepartements = new HashSet<Departement>();
		if (!this.listeDepartements.contains(newDepartement))
			this.listeDepartements.add(newDepartement);
	}

	public void removeListeDepartements(Departement oldDepartement) {
		if (oldDepartement == null)
			return;
		if (this.listeDepartements != null)
			if (this.listeDepartements.contains(oldDepartement))
				this.listeDepartements.remove(oldDepartement);
	}

	public void removeAllListeDepartements() {
		if (listeDepartements != null)
			listeDepartements.clear();
	}

	public Collection<DemandePhoto> getListeDemandePhoto() {
		if (listeDemandePhoto == null)
			listeDemandePhoto = new HashSet<DemandePhoto>();
		return listeDemandePhoto;
	}

	public Iterator<DemandePhoto> getIteratorListeDemandePhoto() {
		if (listeDemandePhoto == null)
			listeDemandePhoto = new HashSet<DemandePhoto>();
		return listeDemandePhoto.iterator();
	}

	public void setListeDemandePhoto(Collection<?> newListeDemandePhoto) {
		removeAllListeDemandePhoto();
		for (Iterator<?> iter = newListeDemandePhoto.iterator(); iter.hasNext();)
			addListeDemandePhoto((DemandePhoto) iter.next());
	}

	public void addListeDemandePhoto(DemandePhoto newDemandePhoto) {
		if (newDemandePhoto == null)
			return;
		if (this.listeDemandePhoto == null)
			this.listeDemandePhoto = new HashSet<DemandePhoto>();
		if (!this.listeDemandePhoto.contains(newDemandePhoto)) {
			this.listeDemandePhoto.add(newDemandePhoto);
			newDemandePhoto.getHistorienDemandePhoto()
					.addListeDemandePhotoHistorien(newDemandePhoto);
		}
	}

	public void removeListeDemandePhoto(DemandePhoto oldDemandePhoto) {
		if (oldDemandePhoto == null)
			return;
		if (this.listeDemandePhoto != null)
			if (this.listeDemandePhoto.contains(oldDemandePhoto)) {
				this.listeDemandePhoto.remove(oldDemandePhoto);
				oldDemandePhoto.getHistorienDemandePhoto()
						.removeListeDemandePhotoHistorien(oldDemandePhoto);
				if (oldDemandePhoto.getPhotographeDemandePhoto() != null)
					oldDemandePhoto
							.getPhotographeDemandePhoto()
							.removeListeDemandePhotoPhotographe(oldDemandePhoto);
			}
	}

	public void removeAllListeDemandePhoto() {
		if (listeDemandePhoto != null)
			listeDemandePhoto.clear();
	}

	public Collection<Utilisateur> getListeUtilisateur() {
		if (listeUtilisateur == null)
			listeUtilisateur = new HashSet<Utilisateur>();
		return listeUtilisateur;
	}

	public Iterator<Utilisateur> getIteratorListeUtilisateur() {
		if (listeUtilisateur == null)
			listeUtilisateur = new HashSet<Utilisateur>();
		return listeUtilisateur.iterator();
	}

	public void setListeUtilisateur(Collection<Utilisateur> newListeUtilisateur) {
		removeAllListeUtilisateur();
		for (Iterator<Utilisateur> iter = newListeUtilisateur.iterator(); iter.hasNext();)
			addListeUtilisateur((Utilisateur) iter.next());
	}

	public void addListeUtilisateur(Utilisateur newUtilisateur) {
		if (newUtilisateur == null)
			return;
		if (this.listeUtilisateur == null)
			this.listeUtilisateur = new HashSet<Utilisateur>();
		if (!this.listeUtilisateur.contains(newUtilisateur))
			this.listeUtilisateur.add(newUtilisateur);
	}

	public void removeListeUtilisateur(Utilisateur oldUtilisateur) {
		if (oldUtilisateur == null)
			return;
		if (this.listeUtilisateur != null)
			if (this.listeUtilisateur.contains(oldUtilisateur))
				this.listeUtilisateur.remove(oldUtilisateur);
	}

	public void removeAllListeUtilisateur() {
		if (listeUtilisateur != null)
			listeUtilisateur.clear();
	}
}