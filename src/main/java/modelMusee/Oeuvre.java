package modelMusee;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Collection;

public class Oeuvre {
	private String nomOeuvre;
	private String descriptionOeuvre;
	public Collection<DemandePhoto> demandePhoto;

	public Collection<Photo> ListePhoto;
	public Departement ListeDepartements;

	public Oeuvre(String nomOeuvre, String descriptionOeuvre) {
		super();
		this.nomOeuvre = nomOeuvre;
		this.descriptionOeuvre = descriptionOeuvre;
	}

	@Override
	public String toString() {
		return nomOeuvre;
	}

	public String getNomOeuvre() {
		return nomOeuvre;
	}

	public void setNomOeuvre(String nomOeuvre) {
		this.nomOeuvre = nomOeuvre;
	}

	public String getDescriptionOeuvre() {
		return descriptionOeuvre;
	}

	public void setDescriptionOeuvre(String descriptionOeuvre) {
		this.descriptionOeuvre = descriptionOeuvre;
	}

	public Oeuvre getOeuvre() {
		return this;
	}

	public Character getPremiereLettreOeuvre() {
		return nomOeuvre.charAt(0);
	}

	public Collection<Photo> getListePhoto() {
		if (ListePhoto == null)
			ListePhoto = new HashSet<Photo>();
		return ListePhoto;
	}

	public Iterator<Photo> getIteratorListePhoto() {
		if (ListePhoto == null)
			ListePhoto = new HashSet<Photo>();
		return ListePhoto.iterator();
	}

	public void setListePhoto(Collection<Photo> newListePhoto) {
		removeAllListePhoto();
		for (Iterator<Photo> iter = newListePhoto.iterator(); iter.hasNext();)
			addListePhoto((Photo) iter.next());
	}

	public void addListePhoto(Photo newPhoto) {
		if (newPhoto == null)
			return;
		if (this.ListePhoto == null)
			this.ListePhoto = new HashSet<Photo>();
		if (!this.ListePhoto.contains(newPhoto))
			this.ListePhoto.add(newPhoto);
	}

	public void removeListePhoto(Photo oldPhoto) {
		if (oldPhoto == null)
			return;
		if (this.ListePhoto != null)
			if (this.ListePhoto.contains(oldPhoto))
				this.ListePhoto.remove(oldPhoto);
	}

	public void removeAllListePhoto() {
		if (ListePhoto != null)
			ListePhoto.clear();
	}

	public Departement getListeDepartements() {
		return ListeDepartements;
	}

	public void setListeDepartements(Departement newDepartement) {
		if (this.ListeDepartements == null
				|| !this.ListeDepartements.equals(newDepartement)) {
			if (this.ListeDepartements != null) {
				Departement oldDepartement = this.ListeDepartements;
				this.ListeDepartements = null;
				oldDepartement.removeListeOeuvres(this);
			}
			if (newDepartement != null) {
				this.ListeDepartements = newDepartement;
				this.ListeDepartements.addListeOeuvres(this);
			}
		}
	}

	public Collection<DemandePhoto> getDemandePhoto() {
		if (demandePhoto == null)
			demandePhoto = new HashSet<DemandePhoto>();
		return demandePhoto;
	}

	public Iterator<DemandePhoto> getIteratorDemandePhoto() {
		if (demandePhoto == null)
			demandePhoto = new HashSet<DemandePhoto>();
		return demandePhoto.iterator();
	}

	public void setDemandePhoto(Collection<?> newDemandePhoto) {
		removeAllDemandePhoto();
		for (Iterator<?> iter = newDemandePhoto.iterator(); iter.hasNext();)
			addDemandePhoto((DemandePhoto) iter.next());
	}

	public void addDemandePhoto(DemandePhoto newDemandePhoto) {
		if (newDemandePhoto == null)
			return;
		if (this.demandePhoto == null)
			this.demandePhoto = new HashSet<DemandePhoto>();
		if (!this.demandePhoto.contains(newDemandePhoto)) {
			this.demandePhoto.add(newDemandePhoto);
			newDemandePhoto.setOeuvre(this);
		}
	}

	public void removeDemandePhoto(DemandePhoto oldDemandePhoto) {
		if (oldDemandePhoto == null)
			return;
		if (this.demandePhoto != null)
			if (this.demandePhoto.contains(oldDemandePhoto)) {
				this.demandePhoto.remove(oldDemandePhoto);
				oldDemandePhoto.setOeuvre((Oeuvre) null);
			}
	}

	public void removeAllDemandePhoto() {
		if (demandePhoto != null) {
			DemandePhoto oldDemandePhoto;
			for (Iterator<DemandePhoto> iter = getIteratorDemandePhoto(); iter
					.hasNext();) {
				oldDemandePhoto = (DemandePhoto) iter.next();
				iter.remove();
				oldDemandePhoto.setOeuvre((Oeuvre) null);
			}
		}
	}
}