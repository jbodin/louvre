package modelMusee;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

/***********************************************************************
 * Module: Departement.java
 * Author: Tim et Joris
 * Purpose: Defines the Class Departement
 ***********************************************************************/

public class Departement {
	private String nomDepartement;
	private String geolocalisationDepartement;

	public Collection<Oeuvre> ListeOeuvres;

	public Departement(String nomDepartement) {
		this.nomDepartement = nomDepartement;
		this.geolocalisationDepartement = "";
		this.ListeOeuvres = null;
	}

	public String getNomDepartement() {
		return this.nomDepartement;
	}

	public String getGeolocalisationDepartement() {
		return geolocalisationDepartement;
	}

	public void setGeolocalisationDepartement(String geolocalisationDepartement) {
		this.geolocalisationDepartement = geolocalisationDepartement;
	}

	@Override
	public String toString() {
		return nomDepartement;
	}

	public ArrayList<Character> getListLettreOeuvre() {
		ArrayList<Character> listeLettre = new ArrayList<Character>();
		for (Iterator<Oeuvre> iter = getIteratorListeOeuvres(); iter.hasNext();) {
			listeLettre.add(((Oeuvre) iter.next()).getPremiereLettreOeuvre());
		}
		return listeLettre;
	}

	public ArrayList<Oeuvre> getListOeuvre(Character c) {
		ArrayList<Oeuvre> listeOeuvre = new ArrayList<Oeuvre>();
		for (Iterator<Oeuvre> iter = getIteratorListeOeuvres(); iter.hasNext();) {
			Oeuvre i = ((Oeuvre) iter.next());
			if (i.getPremiereLettreOeuvre() == c) {
				listeOeuvre.add(i);
			}
		}
		return listeOeuvre;
	}

	public Collection<Oeuvre> getListeOeuvres() {
		if (ListeOeuvres == null)
			ListeOeuvres = new HashSet<Oeuvre>();
		return ListeOeuvres;
	}

	public Iterator<Oeuvre> getIteratorListeOeuvres() {
		if (ListeOeuvres == null)
			ListeOeuvres = new HashSet<Oeuvre>();
		return ListeOeuvres.iterator();
	}

	public void setListeOeuvres(Collection<Oeuvre> newListeOeuvres) {
		removeAllListeOeuvres();
		for (Iterator<Oeuvre> iter = newListeOeuvres.iterator(); iter.hasNext();)
			addListeOeuvres(iter.next());
	}

	public void addListeOeuvres(Oeuvre newOeuvre) {
		if (newOeuvre == null)
			return;
		if (this.ListeOeuvres == null)
			this.ListeOeuvres = new HashSet<Oeuvre>();
		if (!this.ListeOeuvres.contains(newOeuvre)) {
			this.ListeOeuvres.add(newOeuvre);
			newOeuvre.setListeDepartements(this);
		}
	}

	public void removeListeOeuvres(Oeuvre oldOeuvre) {
		if (oldOeuvre == null)
			return;
		if (this.ListeOeuvres != null)
			if (this.ListeOeuvres.contains(oldOeuvre)) {
				this.ListeOeuvres.remove(oldOeuvre);
				oldOeuvre.setListeDepartements((Departement) null);
			}
	}

	public void removeAllListeOeuvres() {
		if (ListeOeuvres != null) {
			Oeuvre oldOeuvre;
			for (Iterator<Oeuvre> iter = getIteratorListeOeuvres(); iter
					.hasNext();) {
				oldOeuvre = iter.next();
				iter.remove();
				oldOeuvre.setListeDepartements((Departement) null);
			}
		}
	}

}