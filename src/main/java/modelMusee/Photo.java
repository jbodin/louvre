package modelMusee;

/***********************************************************************
 * Module:  Photo.java
 * Author:  Tim et Joris
 * Purpose: Defines the Class Photo
 ***********************************************************************/

import java.lang.Byte;
import java.lang.String;

public class Photo {
	private String nomPhoto;
	private Byte photo;

	public Photo(String nomPhoto, Byte photo) {
		super();
		this.nomPhoto = nomPhoto;
		this.photo = photo;
	}

	public String getNomPhoto() {
		return nomPhoto;
	}

	public void setNomPhoto(String nomPhoto) {
		this.nomPhoto = nomPhoto;
	}

	public Byte getPhoto() {
		return photo;
	}

	public void setPhoto(Byte photo) {
		this.photo = photo;
	}

}