package modelMusee;

/***********************************************************************
 * Module:  DemandePhoto.java
 * Author:  Tim et Joris
 * Purpose: Defines the Class DemandePhoto
 ***********************************************************************/

import java.util.*;

import EtatDemande.EtatDemandePhoto;
import EtatDemande.EtatNonPrisEnCharge;

public class DemandePhoto {
	private Date date_demande_photo;

	private Historien historienDemandePhoto;
	private Photographe photographeDemandePhoto;
	private Oeuvre oeuvre;
	private String description;
	private EtatDemandePhoto edp;
	private Collection<Photo> listePhotoDemandePhoto;

	public DemandePhoto(Historien historienDemandePhoto, Oeuvre oeuvre,
			String description) {
		super();
		this.historienDemandePhoto = historienDemandePhoto;
		this.oeuvre = oeuvre;
		this.description = description;
		this.date_demande_photo = Calendar.getInstance().getTime();
		this.photographeDemandePhoto = null;
		this.edp = new EtatNonPrisEnCharge(this);
	}

	public EtatDemandePhoto getEdp() {
		return edp;
	}

	public void setEdp(EtatDemandePhoto edp) {
		this.edp = edp;
	}

	public Date getDate_demande_photo() {
		return date_demande_photo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Historien getHistorienDemandePhoto() {
		return historienDemandePhoto;
	}

	public void prendreEnCharge(Photographe photo) {
		edp.prendreEnCharge(photo);
	}

	public void setHistorienDemandePhoto(Historien newHistorien) {
		if (this.historienDemandePhoto == null
				|| !this.historienDemandePhoto.equals(newHistorien)) {
			if (this.historienDemandePhoto != null) {
				Historien oldHistorien = this.historienDemandePhoto;
				this.historienDemandePhoto = null;
				oldHistorien.removeListeDemandePhotoHistorien(this);
			}
			if (newHistorien != null) {
				this.historienDemandePhoto = newHistorien;
				this.historienDemandePhoto.addListeDemandePhotoHistorien(this);
			}
		}
	}

	public Photographe getPhotographeDemandePhoto() {
		return photographeDemandePhoto;
	}

	public void setPhotographeDemandePhoto(Photographe newPhotographe) {
		if (this.photographeDemandePhoto == null
				|| !this.photographeDemandePhoto.equals(newPhotographe)) {
			if (this.photographeDemandePhoto != null) {
				Photographe oldPhotographe = this.photographeDemandePhoto;
				this.photographeDemandePhoto = null;
				oldPhotographe.removeListeDemandePhotoPhotographe(this);
			}
			if (newPhotographe != null) {
				this.photographeDemandePhoto = newPhotographe;
				this.photographeDemandePhoto
						.addListeDemandePhotoPhotographe(this);
			}
		}
	}

	public Oeuvre getOeuvre() {
		return oeuvre;
	}

	public void setOeuvre(Oeuvre newOeuvre) {
		if (this.oeuvre == null || !this.oeuvre.equals(newOeuvre)) {
			if (this.oeuvre != null) {
				Oeuvre oldOeuvre = this.oeuvre;
				this.oeuvre = null;
				oldOeuvre.removeDemandePhoto(this);
			}
			if (newOeuvre != null) {
				this.oeuvre = newOeuvre;
				this.oeuvre.addDemandePhoto(this);
			}
		}
	}

	public Collection<Photo> getListePhotoDemandePhoto() {
		if (listePhotoDemandePhoto == null)
			listePhotoDemandePhoto = new HashSet<Photo>();
		return listePhotoDemandePhoto;
	}
	
	public Iterator<Photo> getIteratorListePhoto() {
		if (listePhotoDemandePhoto == null)
			listePhotoDemandePhoto = new HashSet<Photo>();
		return listePhotoDemandePhoto.iterator();
	}

	public void setListePhoto(Collection<Photo> newListePhoto) {
		removeAllListePhoto();
		for (Iterator<Photo> iter = newListePhoto.iterator(); iter.hasNext();)
			addListePhoto((Photo) iter.next());
	}

	public void addListePhoto(Photo newPhoto) {
		if (newPhoto == null)
			return;
		if (this.listePhotoDemandePhoto == null)
			this.listePhotoDemandePhoto = new HashSet<Photo>();
		if (!this.listePhotoDemandePhoto.contains(newPhoto))
			this.listePhotoDemandePhoto.add(newPhoto);
	}

	public void removeListePhoto(Photo oldPhoto) {
		if (oldPhoto == null)
			return;
		if (this.listePhotoDemandePhoto != null)
			if (this.listePhotoDemandePhoto.contains(oldPhoto))
				this.listePhotoDemandePhoto.remove(oldPhoto);
	}

	public void removeAllListePhoto() {
		if (listePhotoDemandePhoto != null)
			listePhotoDemandePhoto.clear();
	}
	

	@Override
	public String toString() {
		return oeuvre.getNomOeuvre() + " ["
				+ historienDemandePhoto.getLogin_utilisateur() + "]";
	}

}