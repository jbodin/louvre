package modelMusee;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

/***********************************************************************
 * Module: Historien.java
 * Author: Tim et Joris
 * Purpose: Defines the Class Historien
 ***********************************************************************/

public class Historien extends Utilisateur {

	private Collection<DemandePhoto> listeDemandePhotoHistorien;

	public Historien(int iD_utilisateur, String login_utilisateur,
			String mot_de_passe_utilisateur) {
		super(iD_utilisateur, login_utilisateur, mot_de_passe_utilisateur);
	}

	public Collection<DemandePhoto> getListeDemandePhotoHistorien() {
		if (listeDemandePhotoHistorien == null)
			listeDemandePhotoHistorien = new HashSet<DemandePhoto>();
		return listeDemandePhotoHistorien;
	}

	public Iterator<DemandePhoto> getIteratorListeDemandePhotoHistorien() {
		if (listeDemandePhotoHistorien == null)
			listeDemandePhotoHistorien = new HashSet<DemandePhoto>();
		return listeDemandePhotoHistorien.iterator();
	}

	public void setListeDemandePhotoHistorien(
			Collection<?> newListeDemandePhotoHistorien) {
		removeAllListeDemandePhotoHistorien();
		for (Iterator<?> iter = newListeDemandePhotoHistorien.iterator(); iter
				.hasNext();)
			addListeDemandePhotoHistorien((DemandePhoto) iter.next());
	}

	public void addListeDemandePhotoHistorien(DemandePhoto newDemandePhoto) {
		if (newDemandePhoto == null)
			return;
		if (this.listeDemandePhotoHistorien == null)
			this.listeDemandePhotoHistorien = new HashSet<DemandePhoto>();
		if (!this.listeDemandePhotoHistorien.contains(newDemandePhoto)) {
			this.listeDemandePhotoHistorien.add(newDemandePhoto);
			newDemandePhoto.setHistorienDemandePhoto(this);
		}
	}

	public void removeListeDemandePhotoHistorien(DemandePhoto oldDemandePhoto) {
		if (oldDemandePhoto == null)
			return;
		if (this.listeDemandePhotoHistorien != null)
			if (this.listeDemandePhotoHistorien.contains(oldDemandePhoto)) {
				this.listeDemandePhotoHistorien.remove(oldDemandePhoto);
				oldDemandePhoto.setHistorienDemandePhoto((Historien) null);
			}
	}

	public void removeAllListeDemandePhotoHistorien() {
		if (listeDemandePhotoHistorien != null) {
			DemandePhoto oldDemandePhoto;
			for (Iterator<DemandePhoto> iter = getIteratorListeDemandePhotoHistorien(); iter
					.hasNext();) {
				oldDemandePhoto = (DemandePhoto) iter.next();
				iter.remove();
				oldDemandePhoto.setHistorienDemandePhoto((Historien) null);
			}
		}
	}

}