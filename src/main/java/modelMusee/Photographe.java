package modelMusee;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

/***********************************************************************
 * Module: Photographe.java Author: Tim et Joris Purpose: Defines the Class
 * Photographe
 ***********************************************************************/

public class Photographe extends Utilisateur {

	private Collection<DemandePhoto> listeDemandePhotoPhotographe;

	public Photographe(int iD_utilisateur, String login_utilisateur,
			String mot_de_passe_utilisateur) {
		super(iD_utilisateur, login_utilisateur, mot_de_passe_utilisateur);
	}

	public Collection<DemandePhoto> getListeDemandePhotoPhotographe() {
		if (listeDemandePhotoPhotographe == null)
			listeDemandePhotoPhotographe = new HashSet<DemandePhoto>();
		return listeDemandePhotoPhotographe;
	}

	public Iterator<DemandePhoto> getIteratorListeDemandePhotoPhotographe() {
		if (listeDemandePhotoPhotographe == null)
			listeDemandePhotoPhotographe = new HashSet<DemandePhoto>();
		return listeDemandePhotoPhotographe.iterator();
	}

	public void setListeDemandePhotoPhotographe(
			Collection<?> newListeDemandePhotoPhotographe) {
		removeAllListeDemandePhotoPhotographe();
		for (Iterator<?> iter = newListeDemandePhotoPhotographe.iterator(); iter
				.hasNext();)
			addListeDemandePhotoPhotographe((DemandePhoto) iter.next());
	}

	public void addListeDemandePhotoPhotographe(DemandePhoto newDemandePhoto) {
		if (newDemandePhoto == null)
			return;
		if (this.listeDemandePhotoPhotographe == null)
			this.listeDemandePhotoPhotographe = new HashSet<DemandePhoto>();
		if (!this.listeDemandePhotoPhotographe.contains(newDemandePhoto)) {
			this.listeDemandePhotoPhotographe.add(newDemandePhoto);
			newDemandePhoto.setPhotographeDemandePhoto(this);
		}
	}

	public void removeListeDemandePhotoPhotographe(DemandePhoto oldDemandePhoto) {
		if (oldDemandePhoto == null)
			return;
		if (this.listeDemandePhotoPhotographe != null)
			if (this.listeDemandePhotoPhotographe.contains(oldDemandePhoto)) {
				this.listeDemandePhotoPhotographe.remove(oldDemandePhoto);
				oldDemandePhoto.setPhotographeDemandePhoto((Photographe) null);
			}
	}

	public void removeAllListeDemandePhotoPhotographe() {
		if (listeDemandePhotoPhotographe != null) {
			DemandePhoto oldDemandePhoto;
			for (Iterator<DemandePhoto> iter = getIteratorListeDemandePhotoPhotographe(); iter
					.hasNext();) {
				oldDemandePhoto = (DemandePhoto) iter.next();
				iter.remove();
				oldDemandePhoto.setPhotographeDemandePhoto((Photographe) null);
			}
		}
	}

}