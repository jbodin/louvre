package EtatDemande;

import java.util.Collection;

import UI.Main;
import modelMusee.DemandePhoto;
import modelMusee.Photo;
import modelMusee.Photographe;

public class EtatHonoree implements EtatDemandePhoto {

	private DemandePhoto dp;

	public EtatHonoree(DemandePhoto dp) {
		super();
		this.dp = dp;
	}

	@Override
	public void prendreEnCharge(Photographe photog) {
		System.out.println("Demande photo honor�e ...");
	}

	@Override
	public void validerDemandePhoto() {
		dp.setEdp(new EtatValidee(dp));
		System.out.println("Demande photo valid�e");
	}

	@Override
	public void modifierDemandePhoto(Main main) {
		main.afficheModifDemandeRefusee(dp);
	}

	@Override
	public void validerModifDemandePhoto() {
		dp.setEdp(new EtatPrisEncharge(dp));
		System.out.println("Demande photo modifi�e suite au refus");
	}

	@Override
	public void honorerDemandePhoto(Main main) {
		System.out.println("Demande photo d�j� honor�e ...");
	}

	@Override
	public void validerHonorerDemandePhoto(Collection<Photo> photos) {
		System.out.println("Demande photo d�j� honor�e ...");
	}

	@Override
	public void desengagerDemandePhoto() {
		System.out.println("Demande photo honor�e ...");
	}

}
