package EtatDemande;

import java.util.Collection;

import UI.Main;
import modelMusee.DemandePhoto;
import modelMusee.Photo;
import modelMusee.Photographe;

public class EtatValidee implements EtatDemandePhoto {

	private DemandePhoto dp;

	public EtatValidee(DemandePhoto dp) {
		super();
		this.dp = dp;
	}

	@Override
	public void prendreEnCharge(Photographe photog) {
		System.out.println("Demande photo valid�e ...");
	}

	@Override
	public void validerDemandePhoto() {
		System.out.println("Demande photo valid�e ...");
	}

	@Override
	public void modifierDemandePhoto(Main main) {
		System.out.println("Demande photo valid�e ...");
	}

	@Override
	public void validerModifDemandePhoto() {
		System.out.println("Demande photo valid�e ...");
	}

	@Override
	public void honorerDemandePhoto(Main main) {
		System.out.println("Demande photo valid�e ...");
	}

	@Override
	public void validerHonorerDemandePhoto(Collection<Photo> photos) {
		System.out.println("Demande photo d�j� valid�e ...");
	}

	@Override
	public void desengagerDemandePhoto() {
		System.out.println("Demande photo valid�e ...");
	}

}
