package EtatDemande;

import java.util.Collection;

import UI.Main;
import modelMusee.DemandePhoto;
import modelMusee.Photo;
import modelMusee.Photographe;

public class EtatPrisEncharge implements EtatDemandePhoto {

	private DemandePhoto dp;

	public EtatPrisEncharge(DemandePhoto dp) {
		super();
		this.dp = dp;
	}

	@Override
	public void prendreEnCharge(Photographe photog) {
		System.out.println("Demande photo d�j� prise");
	}

	@Override
	public void validerDemandePhoto() {
		System.out.println("Demande photo d�j� prise");
	}

	@Override
	public void modifierDemandePhoto(Main main) {
		System.out.println("Demande photo d�j� prise");
	}

	@Override
	public void validerModifDemandePhoto() {
		System.out.println("Demande photo d�j� prise");
	}

	@Override
	public void honorerDemandePhoto(Main main) {
		main.afficheHonorerDemande(dp);
	}

	@Override
	public void validerHonorerDemandePhoto(Collection<Photo> photos) {
		dp.setListePhoto(photos);
		dp.setEdp(new EtatHonoree(dp));
		System.out.println("Demande photo honor�e");
	}

	@Override
	public void desengagerDemandePhoto() {
		dp.setPhotographeDemandePhoto(null);
		dp.setEdp(new EtatNonPrisEnCharge(dp));
		System.out.println("Demande photo d�sengag�e");
	}

}
