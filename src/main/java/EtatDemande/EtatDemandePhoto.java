package EtatDemande;

import java.util.Collection;

import modelMusee.Photo;
import modelMusee.Photographe;
import UI.Main;

public interface EtatDemandePhoto {
	
	public void prendreEnCharge(Photographe photog);
	
	public void validerDemandePhoto();
	
	public void modifierDemandePhoto(Main main);
	
	public void validerModifDemandePhoto();
	
	public void honorerDemandePhoto(Main main);
	
	public void validerHonorerDemandePhoto(Collection<Photo> photos);
	
	public void desengagerDemandePhoto();

}
