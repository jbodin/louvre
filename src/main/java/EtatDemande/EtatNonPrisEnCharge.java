package EtatDemande;

import java.util.Collection;

import UI.Main;
import modelMusee.DemandePhoto;
import modelMusee.Photo;
import modelMusee.Photographe;

public class EtatNonPrisEnCharge implements EtatDemandePhoto {

	private DemandePhoto dp;

	public EtatNonPrisEnCharge(DemandePhoto dp) {
		super();
		this.dp = dp;
	}

	@Override
	public void prendreEnCharge(Photographe photog) {
		dp.setPhotographeDemandePhoto(photog);
		photog.addListeDemandePhotoPhotographe(dp);
		dp.setEdp(new EtatPrisEncharge(dp));
		System.out.println("Demande photo accept�");
	}

	@Override
	public void validerDemandePhoto() {
		System.out.println("Demande photo non pris en charge ...");
	}

	@Override
	public void modifierDemandePhoto(Main main) {
		System.out.println("Demande photo non pris en charge ...");
	}

	@Override
	public void validerModifDemandePhoto() {
		System.out.println("Demande photo non pris en charge ...");
	}

	@Override
	public void honorerDemandePhoto(Main main) {
		System.out.println("Demande photo non pris en charge ...");
	}

	@Override
	public void validerHonorerDemandePhoto(Collection<Photo> photos) {
		System.out.println("Demande photo non pris en charge ...");
	}

	@Override
	public void desengagerDemandePhoto() {
		System.out.println("La demande est d�j� sans photographe");
	}

}
